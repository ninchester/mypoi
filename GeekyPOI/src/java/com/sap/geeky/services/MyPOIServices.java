package com.sap.geeky.services;

import com.sap.geeky.db.jdbc.PointOfInterestDBUtils;
import com.sap.geeky.entities.PointOfInterest;
import com.sap.geeky.entities.PointsOfInterest;
import com.sap.geeky.exceptions.DataBaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author nsn
 */
@Controller
public class MyPOIServices {

    private static final String POI_VIEW_KEY = "poiView";
    private static final String OBJECT_POI = "pois";
    private static final String REQUEST_PARAM_X = "x";
    private static final String REQUEST_PARAM_Y = "y";
    private static final String REQUEST_PARAM_DESCRIPTION = "description";
    private static final String RESOURCE_ALL_POIS = "/mypoiall";
    private static final String RESOURCE_POI_BY_ID = "/mypoi/{poiId}";
    private static final String RESOURCE_NEAREST_POIS = "/mypoi";
    private static final String RESOURCE_NEW_POI = "/newpoi";
    @Autowired
    private PointOfInterestDBUtils dbUtils;

    @RequestMapping(value = RESOURCE_ALL_POIS, method = RequestMethod.GET)
    public ModelAndView getAllPointsOfInterest() throws DataBaseException {
        // Returns all POI-s with their ID-s
        PointsOfInterest pois = new PointsOfInterest();
        pois.getPoiList().addAll(dbUtils.fetchAllPointsOfInterest());
        ModelAndView mav = new ModelAndView(POI_VIEW_KEY);
        mav.addObject(OBJECT_POI, pois);
        return mav;
    }

    @RequestMapping(value = RESOURCE_POI_BY_ID, method = RequestMethod.GET)
    public ModelAndView getPointOfInterestById(@PathVariable Long poiId) throws DataBaseException {
        // Returns POI by ID
        PointsOfInterest pois = new PointsOfInterest();
        pois.getPoiList().add(dbUtils.fetchPointOfInterestById(poiId));
        ModelAndView mav = new ModelAndView(POI_VIEW_KEY);
        mav.addObject(OBJECT_POI, pois);
        return mav;
    }

    @RequestMapping(value = RESOURCE_NEAREST_POIS, method = RequestMethod.GET)
    public ModelAndView getPointsOfInterestByCoordinates(@RequestParam(REQUEST_PARAM_X) String x, @RequestParam(REQUEST_PARAM_Y) String y) throws DataBaseException {
        // Returns the closest 5 POI-s 
        PointsOfInterest pois = new PointsOfInterest();
        pois.getPoiList().addAll(dbUtils.fetchNearestPointOfInterest(x, y));
        ModelAndView mav = new ModelAndView(POI_VIEW_KEY);
        mav.addObject(OBJECT_POI, pois);
        return mav;
    }

    @RequestMapping(value = RESOURCE_NEW_POI, method = RequestMethod.PUT)
    public ModelAndView putPointOfInterest(@RequestParam(REQUEST_PARAM_X) String x, @RequestParam(REQUEST_PARAM_Y) String y, @RequestParam(REQUEST_PARAM_DESCRIPTION) String description) throws DataBaseException {
        // Create new POI
        PointOfInterest poi = dbUtils.savePoi(x, y, description);

        PointsOfInterest pois = new PointsOfInterest();
        pois.getPoiList().add(poi);
        ModelAndView mav = new ModelAndView(POI_VIEW_KEY);
        mav.addObject(OBJECT_POI, pois);
        return mav;
    }

    @RequestMapping(value = RESOURCE_POI_BY_ID, method = RequestMethod.DELETE)
    public ModelAndView deletePointOfInterest(@PathVariable Long poiId) throws DataBaseException {
        // Deletes POI by id
        dbUtils.deletePoi(poiId);

        ModelAndView mav = new ModelAndView(POI_VIEW_KEY);
        PointsOfInterest pois = new PointsOfInterest();
        mav.addObject(OBJECT_POI, pois);
        return mav;
    }

    @ExceptionHandler(DataBaseException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public void handleDataBaseException(DataBaseException e) {
        // Return response code 500
    }
}
