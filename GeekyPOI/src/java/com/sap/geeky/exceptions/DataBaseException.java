package com.sap.geeky.exceptions;

/**
 *
 * @author nsn
 */
public class DataBaseException extends Exception {

    public DataBaseException(final String message) {
        super(message);
    }
}
