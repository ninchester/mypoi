/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sap.geeky.beans;

import com.sap.geeky.db.jdbc.PointOfInterestDBUtils;
import com.sap.geeky.entities.PointOfInterest;
import com.sap.geeky.exceptions.DataBaseException;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author nsn
 */
@Named(value="poisBean")
@RequestScoped
public class PoisBean {

    private String url;
    private PointOfInterest choosenPoi;
    @Inject
    private PointOfInterestDBUtils dbUtils;

    public List<PointOfInterest> getPois() throws DataBaseException {
        return dbUtils.fetchAllPointsOfInterest();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public PointOfInterest getChoosenPoi() {
        return choosenPoi;
    }

    public void setChoosenPoi(PointOfInterest choosenPoi) {
        this.choosenPoi = choosenPoi;
    }

    public void recreateGoogleMapsURL(PointOfInterest poi) {
        choosenPoi = poi;
        url = String.format("http://maps.googleapis.com/maps/api/staticmap?center=%s,%s&zoom=12&size=400x300&markers=label:%s|%s,%s&sensor=false", poi.getCoordinateX(), poi.getCoordinateY(), poi.getDescription(), poi.getCoordinateX(), poi.getCoordinateY());

        System.out.println(url);
    }
}
