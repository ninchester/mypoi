package com.sap.geeky.db.jdbc;

import com.sap.geeky.entities.PointDistancePair;
import com.sap.geeky.entities.PointOfInterest;
import com.sap.geeky.exceptions.DataBaseException;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author nsn
 */
@Service
public class PointOfInterestDBUtils {

    private static final String DB_URL = "jdbc:derby:WEB-INF/data/MyPOI;create=true";
    private static final String TABLE_NAME = "POINTSOFINTEREST";
    private static final String CLIENT_DRIVER = "org.apache.derby.jdbc.ClientDriver";
    private static final String SELECT_ALL_POIS = "Select * from %s";
    private static final String SELECT_POI_BY_ID = "Select * from %s where id=%d";
    private static final String INSERT_POI = "insert into %s (XCOORDINATE, YCOORDINATE, DESCRIPTION) values (?, ?, ?)";
    private static final String DELETE_POI = "delete from %s where id=%d";
    private static final String CREATE_POIS_TABLE = "CREATE TABLE %s (ID INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY, XCOORDINATE VARCHAR(20) NOT NULL, YCOORDINATE VARCHAR(20) NOT NULL, DESCRIPTION VARCHAR(255) NOT NULL)";
    private static final int COUNT_OF_NEAREST_POINTS = 5;
    private static final int POWER = 2;
    private Connection connection = null;
    private Statement statement = null;
    private ResultSet results = null;
    private PreparedStatement prepStatement = null;

    private void openConnection() throws DataBaseException {
        try {
            Class.forName(CLIENT_DRIVER).newInstance();
            connection = DriverManager.getConnection(DB_URL);

            statement = connection.createStatement();

            DatabaseMetaData dbmd = connection.getMetaData();
            ResultSet rs = dbmd.getTables(null, null, TABLE_NAME, null);
            if (!rs.next()) {
                createPoisTable();
            }
        } catch (Exception e) {
            Logger.getLogger(PointOfInterestDBUtils.class.getName()).log(Level.SEVERE, null, e);
            throw new DataBaseException(e.getMessage());
        }
    }

    private void createPoisTable() throws DataBaseException {
        try {
            statement = connection.createStatement();
            statement.executeUpdate(String.format(CREATE_POIS_TABLE, TABLE_NAME));
        } catch (SQLException ex) {
            Logger.getLogger(PointOfInterestDBUtils.class.getName()).log(Level.SEVERE, null, ex);
            throw new DataBaseException(ex.getMessage());
        }
    }

    private void closeResorces() throws DataBaseException {
        try {
            if (results != null) {
                results.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
            if (prepStatement != null) {
                prepStatement.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(PointOfInterestDBUtils.class.getName()).log(Level.SEVERE, null, ex);
            throw new DataBaseException(ex.getMessage());
        }
    }

    public List<PointOfInterest> fetchAllPointsOfInterest() throws DataBaseException {
        List<PointOfInterest> pois = new ArrayList<PointOfInterest>();
        try {
            openConnection();
            statement = connection.createStatement();
            results = statement.executeQuery(String.format(SELECT_ALL_POIS, TABLE_NAME));
            while (results.next()) {
                long id = results.getInt(1);
                String xCoordinate = results.getString(2);
                String yCoordinate = results.getString(3);
                String description = results.getString(4);

                PointOfInterest poi = new PointOfInterest(id, xCoordinate, yCoordinate, description);
                pois.add(poi);
            }

        } catch (Exception ex) {
            Logger.getLogger(PointOfInterestDBUtils.class.getName()).log(Level.SEVERE, null, ex);
            throw new DataBaseException(ex.getMessage());
        } finally {
            closeResorces();
        }

        return pois;
    }

    public PointOfInterest fetchPointOfInterestById(long id) throws DataBaseException {
        PointOfInterest poi = null;
        try {
            openConnection();
            statement = connection.createStatement();
            results = statement.executeQuery(String.format(SELECT_POI_BY_ID, TABLE_NAME, id));
            if (results.next()) {
                String xCoordinate = results.getString(2);
                String yCoordinate = results.getString(3);
                String description = results.getString(4);

                poi = new PointOfInterest(id, xCoordinate, yCoordinate, description);
            }
        } catch (Exception ex) {
            Logger.getLogger(PointOfInterestDBUtils.class.getName()).log(Level.SEVERE, null, ex);
            throw new DataBaseException(ex.getMessage());
        }

        return poi;
    }

    public List<PointOfInterest> fetchNearestPointOfInterest(String coordinateX, String coordinateY) throws DataBaseException {
        List<PointDistancePair> pairs = Collections.synchronizedList(new ArrayList<PointDistancePair>());

        List<PointOfInterest> allPois = fetchAllPointsOfInterest();
        for (PointOfInterest poi : allPois) {
            pairs = findNearestPois(pairs, poi, coordinateX, coordinateY);
        }

        List<PointOfInterest> nearestPois = new ArrayList<PointOfInterest>();
        if (pairs != null) {
            for (PointDistancePair pair : pairs) {
                nearestPois.add(pair.getPoi());
            }
        }

        return nearestPois;
    }

    private double calculatePoisDistance(float firstPointX, float firstPointY, float secondPointX, float secondPointY) {
        return Math.sqrt(Math.pow(firstPointX - secondPointX, POWER) + Math.pow(firstPointY - secondPointY, POWER));
    }

    private List<PointDistancePair> findNearestPois(List<PointDistancePair> foundPoints, PointOfInterest candidatePoi, String pointX, String pointY) {
        float currPoiX = Float.parseFloat(candidatePoi.getCoordinateX());
        float currPoiY = Float.parseFloat(candidatePoi.getCoordinateY());
        float searchPoiX = Float.parseFloat(pointX);
        float searchPoiY = Float.parseFloat(pointY);
        double distance = calculatePoisDistance(currPoiX, currPoiY, searchPoiX, searchPoiY);

        if (foundPoints.size() < COUNT_OF_NEAREST_POINTS) {
            foundPoints.add(new PointDistancePair(distance, candidatePoi));
        } else {
            Collections.sort(foundPoints);
            PointDistancePair poiDistancePair = foundPoints.get(foundPoints.size() - 1);
            Double lastDistance = poiDistancePair.getDistance();
            if (distance < lastDistance.doubleValue()) {
                foundPoints.remove(poiDistancePair);
                foundPoints.add(new PointDistancePair(distance, candidatePoi));
            }
        }

        return foundPoints;
    }

    public PointOfInterest savePoi(String coordinateX, String coordinateY, String description) throws DataBaseException {
        PointOfInterest poi = new PointOfInterest();
        poi.setCoordinateX(coordinateX);
        poi.setCoordinateY(coordinateY);
        poi.setDescription(description);

        try {
            openConnection();

            prepStatement = connection.prepareStatement(String.format(INSERT_POI, TABLE_NAME), Statement.RETURN_GENERATED_KEYS);
            prepStatement.setString(1, coordinateX);
            prepStatement.setString(2, coordinateY);
            prepStatement.setString(3, description);

            prepStatement.executeUpdate();

            results = prepStatement.getGeneratedKeys();
            if (results.next()) {
                poi.setId(results.getLong(1));
            }

        } catch (Exception ex) {
            Logger.getLogger(PointOfInterestDBUtils.class.getName()).log(Level.SEVERE, null, ex);
            throw new DataBaseException(ex.getMessage());
        } finally {
            closeResorces();
        }

        return poi;
    }

    public void deletePoi(long id) throws DataBaseException {
        try {
            openConnection();
            statement = connection.createStatement();
            statement.executeUpdate(String.format(DELETE_POI, TABLE_NAME, id));
        } catch (Exception ex) {
            Logger.getLogger(PointOfInterestDBUtils.class.getName()).log(Level.SEVERE, null, ex);
            throw new DataBaseException(ex.getMessage());
        } finally {
            closeResorces();
        }
    }
}
