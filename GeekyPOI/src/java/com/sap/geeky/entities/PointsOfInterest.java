package com.sap.geeky.entities;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nsn
 */
@XmlRootElement(name = "pois")
public class PointsOfInterest {

    private List<PointOfInterest> poiList = new ArrayList<PointOfInterest>();

    @XmlElement(name = "poi")
    public List<PointOfInterest> getPoiList() {
        return poiList;
    }

    public void setPoiList(List<PointOfInterest> poiList) {
        this.poiList = poiList;
    }
}
