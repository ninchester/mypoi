package com.sap.geeky.entities;

/**
 *
 * @author nsn
 */
public class PointDistancePair implements Comparable<PointDistancePair> {

    private double distance;
    private PointOfInterest poi;

    public PointDistancePair() {
        // default constructor
    }

    public PointDistancePair(double distance, PointOfInterest poi) {
        this.distance = distance;
        this.poi = poi;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public PointOfInterest getPoi() {
        return poi;
    }

    public void setPoi(PointOfInterest poi) {
        this.poi = poi;
    }

    @Override
    public int compareTo(PointDistancePair o) {
        if (o == null) {
            return -19;
        }
        
        return Double.compare(distance, o.getDistance());
    }
}
