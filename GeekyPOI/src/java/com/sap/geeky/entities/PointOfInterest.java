package com.sap.geeky.entities;

import java.io.Serializable;

/**
 *
 * @author nsn
 */
public class PointOfInterest implements Serializable {
    
    private long id;
    private String coordinateX;
    private String coordinateY;
    private String description;
    
    public PointOfInterest() {
        // default constructor
    }
    
    public PointOfInterest(long id, String xCoordinate, String yCoordinate, String description) {
        this.id = id;
        this.coordinateX = xCoordinate;
        this.coordinateY = yCoordinate;
        this.description = description;
    }

    public String getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(String coordinateX) {
        this.coordinateX = coordinateX;
    }

    public String getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(String coordinateY) {
        this.coordinateY = coordinateY;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
}
