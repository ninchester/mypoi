package com.sap.geeky.db.jdbc;

import com.sap.geeky.db.*;
import com.sap.geeky.entities.PointOfInterest;
import com.sap.geeky.exceptions.DataBaseException;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author nsn
 */
public class TestPointOfInterestDBUtils {

    private PointOfInterestDBUtils poiDB = null;

    public TestPointOfInterestDBUtils() {
        poiDB = new PointOfInterestDBUtils();
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    @Test
    public void testSave() throws DataBaseException {
        PointOfInterest poi = poiDB.savePoi("34343", "3434534534", "Tralala");
        System.out.println(poi.getId());
    }

    @Test
    public void testFetchAllPointsOfInterest() throws DataBaseException {
        List<PointOfInterest> pois = poiDB.fetchAllPointsOfInterest();
        for (PointOfInterest pointOfInterest : pois) {
            System.out.println(pointOfInterest.getDescription());
        }
    }

    @Test
    public void testFetchPointOfInterestById() throws DataBaseException {
        PointOfInterest poi = poiDB.fetchPointOfInterestById(1);
        System.out.println(poi.getId() + poi.getDescription());
    }

    @Test
    public void testDelete() throws DataBaseException {
        poiDB.deletePoi(31);
    }

    @Test
    public void testFetchNearestPointOfInterest() throws DataBaseException {
        List<PointOfInterest> pois = poiDB.fetchNearestPointOfInterest("48.8583", "4");
        for (PointOfInterest pointOfInterest : pois) {
            System.out.println(pointOfInterest.getId());
        }
    }
}
