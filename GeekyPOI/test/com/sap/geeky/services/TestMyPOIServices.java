package com.sap.geeky.services;

import com.sap.geeky.entities.PointOfInterest;
import com.sap.geeky.entities.PointsOfInterest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author nsn
 */
public class TestMyPOIServices {

    RestTemplate restTemplate;

    @Before
    public void setUp() {
        restTemplate = new RestTemplate();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void fetchAllPois() {
        PointsOfInterest pois = (PointsOfInterest) restTemplate.getForObject("http://localhost:8080/GeekyPOI/services/mypoiall", PointsOfInterest.class);
        for (PointOfInterest point : pois.getPoiList()) {
            System.out.println(point.getId());
        }
    }

    @Test
    public void fetchPoiById() {
        PointsOfInterest pois = (PointsOfInterest) restTemplate.getForObject("http://localhost:8080/GeekyPOI/services/mypoi/100", PointsOfInterest.class);
        for (PointOfInterest point : pois.getPoiList()) {
            System.out.println(point.getId());
        }
    }

    @Test
    public void fetchNearestPoisByCoordinates() {
        PointsOfInterest pois = (PointsOfInterest) restTemplate.getForObject("http://localhost:8080/GeekyPOI/services/mypoi?x=48.8583&y=2.2945", PointsOfInterest.class);
        for (PointOfInterest point : pois.getPoiList()) {
            System.out.println(point.getId());
        }
    }

    @Test
    public void putPointOfInterest() {
        restTemplate.put("http://localhost:8080/GeekyPOI/services/newpoi?x=1&y=2&description=Test", PointsOfInterest.class);
    }

    @Test
    public void deletePointOfInterest() {
        restTemplate.delete("http://localhost:8080/GeekyPOI/services/mypoi/34", PointsOfInterest.class);
    }
}
